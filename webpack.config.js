const webpack = require('webpack')
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')

module.exports = {
  mode: 'development',
  entry: './src/index.js',
  devtool: 'cheap-module-eval-source-map',
  output: {
    publicPath: '/build/',
    path: path.resolve(__dirname, 'build'),
    filename: '[name].bundle.js',
  },
  module: {
    rules: [
      { test: /\.jsx?$/, use: 'babel-loader' },
      { test: /\.jsx?$/, use: 'eslint-loader' },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: ['file-loader'],
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: ['file-loader'],
      },
    ],
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    alias: {
      utils$: path.resolve(__dirname, './src/utils.js'),
    },
  },
  devServer: {
    contentBase: 'build',
    open: true,
    openPage: 'build/index.html',
    inline: true,
    hot: true,
    overlay: {
      errors: true,
      warnings: true,
    },
    noInfo: true,
    port: 3000,
  },
  watchOptions: { ignored: ['build', 'node_modules'] },
  plugins: [
    new CleanWebpackPlugin(['build']),
    new HtmlWebpackPlugin({
      title: 'Javascript Starter',
      template: 'index.html',
    }),
    new webpack.HotModuleReplacementPlugin(),
  ],
}
