import { range } from 'rxjs'
import { map, filter, reduce } from 'rxjs/operators'
import { assert } from 'tcomb'

import './style.css'

range(1, 10)
  .pipe(
    filter(x => x % 2 === 0),
    map(x => x * x),
    reduce((x, y) => x + y),
  )
  .subscribe(x => assert(x === 220))

if (module.hot !== undefined) {
  module.hot.accept()
}
