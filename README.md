# Javascript Starter

Javascript development starter using [eslint](http://eslint.org/) for linting, [eslint-config-airbnb-base](https://www.npmjs.com/package/eslint-config-airbnb-base) for default eslint configuration, [babel](https://babeljs.io) for ES6 transpilation, [webpack].(http://webpack.github.io/docs/) for module bundling, , [prettier](https://github.com/prettier/prettier) for formatting, [jest](https://facebook.github.io/jest/) for unit testing, [webpack](https://webpack.js.org/) for module bundling, [prettier](https://github.com/prettier/prettier) for formatting and [webpack-dev-server](https://webpack.js.org/configuration/dev-server/) as live reloading static http server.

## Usage

1.  Install [nodejs](https://nodejs.org/en/). I highly recommend using [nvm](https://github.com/creationix/nvm). If you are on mac or ubuntu, you could use the following command to install nodejs. It's from my [dotfiles](https://gitlab.com/seartipy/dotfiles) configuration.

    On Mac

        curl -L j.mp/srtpldf > setup && bash setup web

    On Ubuntu

        wget -qO- j.mp/srtpldf > setup && bash setup web

2.  Clone this repository and install npm packages. Make sure you have [git](https://git-scm.com/) installed.

        npm install -g yarn
        git clone --depth=1 https://gitlab.com/pervezfunctor/javascript-starter.git
        cd javascript-starter && rm -rf .git
        yarn

3.  Start server

        yarn start

    Now you can edit `src/index.js` in any editor and see your changes in browser immediately.

    You could use `yarn test` to run jest tests.
    You could use `yarn run lint` for linting all files in `src` folder.

4.  Setup your editor

    If you use `visual studio code`, install `visual studio code extensions` using the following command( or use the script in Step 1).

        code --install-extension dbaeumer.vscode-eslint
        code --install-extension msjsdiag.debugger-for-chrome
        code --install-extension esbenp.prettier-vscode

    On Mac, you might have to [install shell command](https://code.visualstudio.com/docs/setup/mac).

## License

MIT
